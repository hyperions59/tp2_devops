# TODO : Create a s3 bucket with aws_s3_bucket

resource "aws_s3_bucket" "s3_job_offer_bucket" {
  bucket        = "s3-job-offer-bucket-leroy"
  acl           = "private"
  force_destroy = true

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}
# TODO : Create 1 nested folder :  job_offers/raw/  |  with  aws_s3_bucket_object

resource "aws_s3_bucket_object" "object" {
  bucket = "s3-job-offer-bucket-leroy"
  key    = "job_offers/raw/"
  source = "/dev/null"

  # The filemd5() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the md5() function and the file() function:
  # etag = "${md5(file("path/to/file"))}
  #etag = filemd5("/dev/null")
}

# TODO : Create an event to trigger the lambda when a file is uploaded into s3 with aws_s3_bucket_notification

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = "s3-job-offer-bucket-leroy"

  lambda_function {
    lambda_function_arn = aws_lambda_function.test_lambda.arn
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = "job_offers/raw/"
    filter_suffix       = ".csv"
  }

  depends_on = [aws_lambda_permission.allow_bucket]
}